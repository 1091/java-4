import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
/**
 * ������������ ��� ������ � �������� double[] {@link massD1}:<br/>
 * 1) ������ ������������� � ���� � ��������������� � ������ double[] {@link massD2}<br/>
 * 2) ������ �������� ���������� ��� ������ ��������� {@link massD1}<br/>
 * 3) ��������� ������� ��������� ��������� �������,<br/>
 *    � ����������������� ������ � ������ �������
 * @author ���� �������, ������� �������
 */
public class Serialization{
	/** ���� ��� �������� ���� � ����� */
	private String fileName;
	/** ���� ��� �������� ��������*/
	double[] massD1, massD2 = new double[361], massD3 = new double[361];
	/**
	 * �������������� ����:<br/>
	 * {@link fileName}, {@link massD1}
	 * @param {@link fileName} = String st
 	 * @param {@link massD1} = double[] massD
	 */
	Serialization(String st, double[] massD){
		fileName=st;
		massD1 = massD;
	}
	/**	��������� �������� */
	public void Run(){
		Serial();	deSerial();
		
		for (int i = 0;  i<= 360; i++)
			System.out.printf("%3d   |   %20.17f   |   %20.17f   |   %20.17f\n", i, massD1[i], massD2[i], massD3[i]);
	}
	/**
	 * 1) ������ {@link massD1} ������������� � ���� (� �������� 1 � ��������)<br/>
	 * 2) ������ {@link massD1} ������������� � ���� (� �������� 2 � ��������) �� ���������<br/>
	 */
	private void Serial(){
		String S =  fileName + "1.dat";
		/** ������ � ���� ����� ������� */
      	try {
			FileOutputStream fout  =  new  FileOutputStream (S);
			ObjectOutputStream oos =  new  ObjectOutputStream (fout); 
	        oos.writeObject(massD1);
	        oos.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
      	/** ������ � ���� �� ������ ����� */
		S =  fileName + "2.dat";
      	try {	
      		FileOutputStream fout  =  new  FileOutputStream (S);
      		fout.close();
			fout  =  new  FileOutputStream (S, true);
			ObjectOutputStream oos =  new  ObjectOutputStream (fout); 
			for (int i = 0;  i<= 360; i++)
	        	oos.writeObject(massD1[i]);
	        oos.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	/**
	 * 1) ������������� ������ �� ����� (� �������� 1 � ��������) � ������ {@link massD2}<br/>
	 * 2) ������������� ������ �� ��������� �� ����� (� �������� 2 � ��������) � ������ {@link massD3}<br/>
	 */
	private void deSerial(){
		String S =  fileName + "1.dat";
		/** ������ �� ����� ����� ������� */
		try {
			FileInputStream streamIn = new FileInputStream(S);
			ObjectInputStream ois = new ObjectInputStream(streamIn);
			massD2 = (double[]) ois.readObject();
	        ois.close();
	    } 
		catch (Exception e) {
	        e.printStackTrace();
		}
      	/** ������ �� ����� �� ������ ����� */
		S =  fileName + "2.dat";
		try {
			FileInputStream streamIn = new FileInputStream(S);
			ObjectInputStream ois = new ObjectInputStream(streamIn);
			for (int i = 0;  i<= 360; i++)
	        	massD3[i] = (double) ois.readObject();
	        ois.close();
	    } 
		catch (Exception e) {
	        e.printStackTrace();
		}
	}
}
