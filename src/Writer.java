import java.io.FileWriter;
import java.io.IOException;
/**
 * ������������ ��� ������ � ��������� ����<br/>
 * �������� ������� ����� �� 0 �� 360 � ����� 1<br/>
 * (������ �������� ������������ � ����� ������)
 * @author ���� �������, ������� �������
 */
public class Writer {
	/** ���� ��� �������� ���� � ����� */
	private String fileName;
	/**
	 * �������������� ����
	 * {@link fileName}
	 * @param {@link fileName} = String st
	 */
	Writer(String st){
		fileName=st;
	}
	/**	��������� �������� */
	void Run(){
		try {
			FileWriter myFile = new FileWriter(fileName);
			for (int i=0; i<=360; i++)
				myFile.write(Double.toString(Math.sin(Math.toRadians(i))) + "\r\n");
			myFile.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
