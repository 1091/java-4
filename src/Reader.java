import java.io.BufferedReader;
import java.io.FileReader;
/**
 * ������������ ��� ������ �� ����� {@link fileName1}<br/>
 * �������� ������� ����� �� 0 �� 360 � ����� 1<br/>
 * (������ �������� �������� � ����� ������)<br/>
 * � �� ����������� ���� �� ����� {@link fileName2} <br/>
 * ������� �� ����� ����� ����� ����
 * @author ���� �������, ������� �������
 */
public class Reader {
	/** ���� ��� �������� ���� � ����� */
	private String fileName1, fileName2;
	/** ���� ��� �������� ���������� ���� �� ����� {@link fileName2}*/
	private int qwest = 0;
	/**
	 * �������������� ����:<br/>
	 * {@link fileName1}, {@link fileName2}<br/>
	 * � ��������� �������� ���� {@link qwest}
	 * @param {@link fileName1} = String st1 
	 * @param {@link fileName2} = String st2
	 */
	public Reader(String st1, String st2){
		fileName1 = st1;
		fileName2 = st2;
		readQwest();
	}
	/** ��������� ��������
	 *  @return ������ double[] 
	 */
	public double[] Run(){
		double[] massD = new double[361];
		try {
			FileReader fr = new FileReader(fileName1);
			BufferedReader in = new BufferedReader(fr);
						
			for (int i = 0; i<=360; i++)
				massD[i] = Double.parseDouble(in.readLine());		
			System.out.println("����� ���� "+ qwest + " ����� " + massD[qwest] + "\n");
			in.close();
			return massD;
		}
		catch(Exception ex) {
			return null;
		}
	}
	private void readQwest(){
		try {
			FileReader fr = new FileReader(fileName2);
			BufferedReader in = new BufferedReader(fr);
			String S = in.readLine();
			qwest = Integer.parseInt(S);
			in.close();
		}
		catch(Exception ex) {}
	}
}
